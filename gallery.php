<?php

use model\Image;

require_once 'config/config.php';
require_once 'model/Image.php';
$image = new Image();

$page_title = 'Gallery';
include_once 'header.php';
include_once 'pagination_home.php';
include_once 'template-parts/selectActive.php';
include_once 'template-parts/modal_delete.php';
include_once 'template-parts/modal_update.php';
include_once 'template-parts/modal-view.php';
include_once 'service/deleteImage.php';
include_once 'service/updateImage.php';
include_once 'footer.php';
