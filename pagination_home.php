<?php

include_once 'model/Image.php';

$rows_total = (int)$image->countRowsActive()[0]['COUNT(*)'];

if (isset($_GET['page'])) {
    $currentPage = $_GET['page'];
} else {
    $currentPage = 1;
}

$s = htmlspecialchars($_SERVER['PHP_SELF']);
$rows_per_page = 4;
$offset = ($currentPage - 1) * $rows_per_page;
$total_pages = ceil($rows_total / $rows_per_page);
$prev = $currentPage - 1;
$next = $currentPage + 1;

$stmt = $image->selectRangeActive($offset, $rows_per_page);

$pag_output = '';
if ($currentPage > 1) {
    // go to 1st
    $pag_output .= '<a class="btn btn-dark m-1" href="' . $s . '?page=1"><i class="fas fa-angle-double-left"></i></a>';
    // go to previous
    $pag_output .= '<a class="btn btn-dark m-1"  href="' . $s . '?page=' . $prev . '"><i class="fas fa-angle-left"></i></a>';
}
if ($currentPage < $total_pages) {
    // go to next
    $pag_output .= '<a class="btn btn-dark m-1"  href="' . $s . '?page=' . $next . '"><i class="fas fa-angle-right"></i></a>';
    // go to last
    $pag_output .= '<a class="btn btn-dark m-1"  href="' . $s . '?page=' . $total_pages . '"><i class="fas fa-angle-double-right"></i></a>';
}

if ($rows_total !== 0) {
    $pag_output .= '<br>' . $currentPage . ' of ' . $total_pages . ' page(s). Total active images: ' . $rows_total;
}

return $pag_output;
