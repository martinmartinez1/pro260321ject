<?php

namespace model;

use PDO;
use FFI\Exception;
use PDOException;

require_once 'config/config.php';
class Database
{
    private static $instance;

    public function __clone()
    {
        throw new Exception('new instance can\'t be raised');
    }

    public function __construct()
    {
    }

    private static function getInstance()
    {
        if (null === self::$instance) {
            $className = __CLASS__;
            self::$instance = new $className();
        }
        return self::$instance;
    }

    public static function getDbConnection()
    {
        try {
            $db = self::getInstance();
            $db = new PDO(
                'mysql:host=' . getDbData()['db_host'] . ';db_name=' . getDbData()['db_name'],
                getDbData()['db_user'],
                getDbData()['db_pass']
            );
            return $db;
        } catch (PDOException $e) {
            echo 'unable to connect' . $e->getMessage();
            return null;
        }
    }
}
