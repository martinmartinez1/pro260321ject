<?php

namespace model;

use Exception;
use PDOException;
use Error;
use PDO;
use model\Database;

require_once 'model/Database.php';
require_once 'config/config.php';
class Image
{
    private int $id;
    private string $url;
    private string $alt;
    private string $title;
    private int $position;
    private int $is_active;
    private int $width;
    private int $height;
    private float $size;
    private string $mime;

    private int $allowedSize = 10;

    private $table_name = 'pro260321ject.images';

    public function __construct()
    {
        $a = new Database();
        $db = $a->getDbConnection();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getId()
    {
        return $this->id;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function getAlt()
    {
        return $this->alt;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getPosition()
    {
        return $this->position;
    }
    public function getIsActive()
    {
        return $this->is_active;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getHeight()
    {
        return $this->height;
    }
    public function getSize()
    {
        return $this->size;
    }
    public function getMime()
    {
        return $this->mime;
    }
    public function getAllowedSize()
    {
        return $this->allowedSize;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setUrl($url)
    {
        $this->url = $url;
    }
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setPosition($position)
    {
        $this->position = $position;
    }
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }
    public function setWidth($width)
    {
        $this->width = $width;
    }
    public function setHeight($height)
    {
        $this->height = $height;
    }
    public function setSize($size)
    {
        $this->size = $size;
    }
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    public static function prepQuery($sql)
    {
        $a = new Database();
        $db = $a->getDbConnection();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function insertUpdateStuff($size, $width, $height, $mime)
    {

        // alt
        isset($_POST['alt'])
            ? (is_string($_POST['alt']) ? $this->setAlt(strval($_POST['alt']))
                : $this->setAlt(""))
            : $this->setAlt("");

        // title
        isset($_POST['title'])
            ? (is_string($_POST['title']) ? $this->setTitle(strval($_POST['title']))
                : $this->setTitle(""))
            : $this->setTitle("");

        // position
        if (isset($_POST['position'])) {
            if (intval($_POST['position']) > 9999) {
                $this->setPosition(9999);
            } else {
                $this->setPosition(intval($_POST['position']) ?: 1);
            }
        } else {
            $this->setPosition(1);
        }

        // active
        $this->setIsActive(isset($_POST['is_active']) ? 1 : 0);

        $this->setWidth(intval($width));
        $this->setHeight(intval($height));
        $this->setSize($size);
        $this->setMime(strval($mime));
        return $this;
    }

    public function countRows()
    {
        // for pagination
        $sql = "SELECT COUNT(*) FROM " . $this->table_name . "";
        $stmt = self::prepQuery($sql);
        $num = $stmt->fetchAll();
        return $num;
    }

    public function countRowsActive()
    {
        $sql = "SELECT COUNT(*) FROM " . $this->table_name . " WHERE is_active = 1";
        $stmt = self::prepQuery($sql);
        $num = $stmt->fetchAll();
        return $num;
    }

    // public function selectRange($selectStart, $selectStop)
    // {
    //     $sql = "SELECT * FROM " . $this->table_name . " ORDER BY id LIMIT $selectStart, $selectStop ";
    //     $stmt = self::prepQuery($sql);
    //     return $stmt;
    // }

    public function selectRangeActive($selectStart, $selectStop)
    {
        $sql = "SELECT * FROM " . $this->table_name . " 
        WHERE is_active = 1 
        ORDER BY position 
        LIMIT $selectStart, $selectStop ";
        $stmt = self::prepQuery($sql);
        return $stmt;
    }

    public function content($var)
    {
        $info = '<div class="alert alert-primary">
            Empty here. Upload some image or set active to move it to the gallery.
            </div>';
        $check_rows = $this->countRows()[0]['COUNT(*)'];
        $check_rows_active = $this->countRowsActive()[0]['COUNT(*)'];

        if (0 == $check_rows && $_SERVER['REQUEST_URI'] == '/pro260321ject/index.php') {
            echo $info;
            include_once 'content_add_image.php';
        } elseif (0 == $check_rows_active && $_SERVER['REQUEST_URI'] == '/pro260321ject/gallery.php') {
            echo $info;
            include_once 'content_add_image.php';
        } else {
            echo $var;
        }
    }

    public function selectAll()
    {
        $sql = "SELECT * FROM " . $this->table_name . "";
        $stmt = self::prepQuery($sql);
        return $stmt;
    }

    public function selectActive()
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE is_active = 1 ORDER BY position ASC";
        $stmt = self::prepQuery($sql);
        return $stmt;
    }

    public function saveFile()
    {
        if (!empty($_FILES['image']['tmp_name'])) {
            $path = get_include_path() . 'upload/standard/';
            $img_url = $path . basename($_FILES['image']['name']);

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }

            $mime_list = array('image/bmp', 'image/gif', 'image/vnd.microsoft.icon', 'image/jpeg',
            'image/jpg', 'image/svg+xml', 'image/tiff', 'image/webp', 'image/png');

            if (in_array($_FILES['image']['type'], $mime_list)) {
                $mb = 1048576;
                $size = floatval(round((($_FILES['image']['size']) / $mb), 2));
                if ($size < $this->allowedSize) {
                    // if size ok
                    if (file_exists($img_url)) {
                        // if exists -> rename
                        while (file_exists($img_url)) {
                            // rename
                            $ext = pathinfo($img_url, PATHINFO_EXTENSION);
                            $img_url = trim($img_url, '.' . $ext);
                            $img_url .= '1.' . $ext;
                        }
                    }
                    // try to move to dir
                    try {
                        move_uploaded_file($_FILES['image']['tmp_name'], $img_url);
                        $img_url = trim($img_url, get_include_path());
                        return [
                            $size,
                            $img_url
                        ];
                        // return $img_url;
                    } catch (Exception $e) {
                        echo $e->getMessage();
                        die;
                    }
                } else {
                    // warning: too big
                    header("Refresh:2");
                    echo '<div class="alert alert-danger">
                    Only files up to 10 MB.  Wait...
                    </div>';
                }
            } else {
                // warning: not an image
                header("Refresh:2");
                echo '<div class="alert alert-danger">
                Only images allowed.  Wait...
                </div>';
                die;
            }
        } else {
            if (!isset($_POST['update'])) {
                // warning: no image attached
                header("Refresh:2");
                echo '<div class="alert alert-danger">
                Choose some image.  Wait...
                </div>';
            }
        }
    }

    private function bind($sql, $db)
    {
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':url', $this->url);
        $stmt->bindParam(':alt', $this->alt);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':position', $this->position);
        $stmt->bindParam(':is_active', $this->is_active);
        $stmt->bindParam(':width', $this->width);
        $stmt->bindParam(':height', $this->height);
        $stmt->bindParam(':size', $this->size);
        $stmt->bindParam(':mime', $this->mime);
        return $stmt;
    }

    public function insert($data)
    {
        $a = new Database();
        $db = $a->getDbConnection();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "INSERT INTO " . $this->table_name . " 
        (url, alt, title, position, is_active, width, height, size, mime) 
        VALUES (:url, :alt, :title, :position, :is_active, :width, :height, :size, :mime);";

        try {
            $this->bind($sql, $db)->execute($data);
        } catch (PDOException $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function update($id)
    {
        $a = new Database();
        $db = $a->getDbConnection();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "UPDATE " . $this->table_name . " 
        SET url=:url, alt=:alt, title=:title, position=:position, 
        is_active=:is_active, width=:width, height=:height, 
        size=:size, mime=:mime 
        WHERE id=" . $id . "";

        try {
            $this->bind($sql, $db)->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function removeFile($url)
    {
        if (isset($url)) {
            unlink($url);
        }
    }

    public function delete($id)
    {
        $a = new Database();
        $db = $a->getDbConnection();

        $sql = "DELETE FROM " . $this->table_name . " WHERE id=:id";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);

        try {
            $stmt->execute();
            // return true;
        } catch (Error $e) {
            echo $e->getMessage();
        }
    }
}
