<?php

use model\Image;

require_once 'config/config.php';
require_once 'model/Image.php';

$page_title = 'Images';
require_once 'header.php';
$image = new Image();
?>

<div class="m-3">

    <?php
    require_once 'pagination_home.php';
    require_once 'template-parts/selectImage.php';
    require_once 'template-parts/modal-view.php';
    require_once 'template-parts/modal_update.php';
    require_once 'template-parts/modal_delete.php';
    require_once 'service/deleteImage.php';
    require_once 'service/updateImage.php';

    require_once 'footer.php';
