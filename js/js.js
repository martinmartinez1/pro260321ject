// pass data to update-modal
$(document).on("click", ".open-modal-update", function () {

    $("#old_url_img").attr('src', $(this).data('url'));
    $("#alt").val($(this).data('alt'));
    $("#title").val($(this).data('title'));
    $("#position").val($(this).data('position'));
    if ($(this).data('is_active') == 1) {
        $("#is_active").prop('checked', true);
    }

    // hidden inputs:
    $("#old_url").val($(this).data('url'));
    $('#old_img_width_hidden').val($(this).data('width'));
    $('#old_img_height_hidden').val($(this).data('height'));
    $('#old_img_size_hidden').val($(this).data('size'));
    $('#old_img_mime_hidden').val($(this).data('mime'));
    $('#id').val($(this).data('id'));
});

// pass data to delete-modal
$(document).on("click", ".open-modal-delete", function () {
    $("#delete_id").val($(this).data('delete_id'));
    $("#delete_url").val($(this).data('delete_url'));
});

// pass data to view-modal
$(document).on("click", ".open-modal-view", function () {
    $("#img-to-show").attr('src', $(this).data('open_img'));
});
