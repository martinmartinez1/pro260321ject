<?php

use model\Image;

require_once 'config/config.php';
require_once 'model/Image.php';
$image = new Image();

$page_title = 'Add an image';
include_once 'header.php';

if ($_POST) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $process = $image->saveFile();
    if (null != $process) {
        $url = $process[1];
        $size = $process[0];
        $image->setUrl($url);
        $imgArr = getimagesize('upload/standard/' . $_FILES['image']['name']);
        $width = $imgArr[0];
        $height = $imgArr[1];
        $mime = $_FILES['image']['type'];

        $image->insertUpdateStuff($size, $width, $height, $mime);

        $data = array(
            'url'       =>  $image->getUrl(),
            'alt'       =>  $image->getAlt(),
            'title'     =>  $image->getTitle(),
            'position'  =>  $image->getPosition(),
            'is_active' =>  $image->getIsActive(),
            'width'     =>  $image->getWidth(),
            'height'    =>  $image->getHeight(),
            'size'      =>  $image->getSize(),
            'mime'      =>  $image->getMime()
        );
        $image->insert($data);
        header("Refresh:2; url=index.php");
        echo '
        <div class="alert alert-success">
        Uploaded.  Wait...
        </div>
        ';
    }
}
?>

<table class="table col-md-6 mx-auto" id="imagesAdd">
    <thead>
        <tr>
            <td colspan="2">
                <h6>Add new image:</h6>
                <p>Only images up to 10MB allowed.</p>
            </td>
        </tr>
    </thead>
    <tbody>
        <form id="formAddImage" enctype="multipart/form-data" method="post" action="
        <?php htmlspecialchars($_SERVER['PHP_SELF']) ?>
        ">
            <tr>
                <td>
                    <div class="form-group">
                        <textarea class="form-control" type="text" 
                        name="alt" rows="3" placeholder="Alt attributes" maxlength="125"></textarea>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input class="form-control" type="text" name="title" placeholder="Title" maxlength="60">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <input type="number" name="position" min="1" max="9999">
                        <label for="position">position on listing</label>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <input class="checkbox" type="checkbox" name="is_active">
                        <label for="is_active">is active</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <input class="form-control-file" type="file" size="32" name="image" multiple accept="image/*">
                    </div>
                </td>
                <td>
                    <input type="submit" class="btn btn-dark m-1 fa-input" value="&#xf00c Submit">
                    <input type="reset" class="btn btn-dark m-1 fa-input" value="&#xf410 Cancel">
                </td>
            </tr>
    </tbody>
    </form>
</table>
</div>
<?php

include_once 'footer.php'; ?>