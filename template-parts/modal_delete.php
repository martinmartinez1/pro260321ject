<div class="container-fluid">

    <div class="modal delete-modal" role="dialog" 
    aria-labelledby="DeleteModal" aria-hidden="true" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p>Image will be removed</p>
                </div>
                <div class="p-3">
                    <form action="
                    <?php htmlspecialchars($_SERVER['PHP_SELF']) ?>
                    " method="POST">
                        <input type="hidden" name="delete_id" id="delete_id" value="">
                        <input type="hidden" name="delete_url" id="delete_url" value="">
                        <input type="submit" class="btn btn-dark" value="Delete" name="delete">
                    <button class="btn btn-light" data-dismiss="modal">
                        Cancel
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>