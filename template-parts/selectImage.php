<?php

use model\Image;

require_once 'model/Image.php';
require_once 'model/Database.php';

$image = new Image();
$stmt = $image->selectAll();

if (null != $stmt) {
    $output = '
    <h6>Previously uploaded:</h6>
    <tr><td colspan="12">
    </td></tr>
    <table class="table text-center table-sm table-striped">
    <thead>
        <th>#</th>
        <th>url</th>
        <th>alt</th>
        <th>position</th>
        <th>active</th>
        <th>width</th>
        <th>height</th>
        <th>size</th>
        <th>mime</th>
        <th>image</th>
        <th>action</th>
    </thead>
        <tbody class="text-monospace">
        ';
    $i = 1;
    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $output .=
            <<<EOD
            <tr>
            <td>$i</td>
            <td class="text-truncate">
            <span title="$row->url" class="d-inline-block text-truncate" style="max-width: 150px;">
            $row->url
            </span></td>
            <td>$row->alt</td>
            <td>$row->position</td>
            <td>$row->is_active</td>
            <td>$row->width</td>
            <td>$row->height</td>
            <td>$row->size</td>
            <td>$row->mime</td>
            <td>
            <div class"row">
            <img 
            data-toggle="modal"
            data-target=".view-modal"
            data-open_img="$row->url"
            class="open-modal-view" src="$row->url" style="width:200px">
            <div class"row">
            <div class="caption">$row->title</div>
            </div>
            </td>
            <td>
            <a
            data-url="$row->url" 
            data-alt="$row->alt" 
            data-title="$row->title" 
            data-position="$row->position" 
            data-is_active="$row->is_active" 
            data-width="$row->width" 
            data-height="$row->height" 
            data-size="$row->size" 
            data-mime="$row->mime" 
            data-id="$row->id" class="btn btn-dark m-1 fa-input open-modal-update" 
            data-toggle="modal" 
            data-target=".update-modal" 
            title="Update"><span>&#xf044</span></a>
            <a data-delete_id="$row->id" data-delete_url="$row->url" 
            class="btn btn-dark m-1 fa-input open-modal-delete" 
            data-toggle="modal" 
            data-target=".delete-modal" title="Delete"><span>&#xf2ed</span></a>
            </td>
            </tr>
            EOD;
        $i++;
    }
    $output .= '
    <tr><td colspan="12">
    </td></tr>
    </tbody>
    </table>
        ';
}

ob_start();
$image->content($output);
