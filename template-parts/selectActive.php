<?php

$stmt = $image->selectRangeActive($offset, $rows_per_page);

if (null != $stmt) {

        $output = '
        <h6>Shows only active images:</h6>
        <tr><td colspan="12">
        <div class="row col-md-8">' . $pag_output . '</div>
        </td></tr>
        <table id="gallery" class="table text-center table-sm table-striped">
        <thead>
        <th>#</th>
        <th>position</th>
        <th>image</th>
        <th>action</th>
        </thead>
        <tbody class="text-monospace">
        ';
        $i = 1;
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $output .=
                        <<<EOD
            <tr>
            <td>$i</td>
            <td>$row->position</td>
            <td>
            <img 
            data-toggle="modal"
            data-target=".view-modal"
            data-open_img="$row->url"
            class="open-modal-view" src="$row->url" style="width:500px">
            <td>
            <a 
            data-url="$row->url" 
            data-alt="$row->alt" 
            data-title="$row->title" 
            data-position="$row->position" 
            data-is_active="$row->is_active" 
            data-width="$row->width" 
            data-height="$row->height" 
            data-size="$row->size" 
            data-mime="$row->mime" 
            data-id="$row->id" class="btn btn-dark m-1 fa-input open-modal-update" 
            data-toggle="modal" 
            data-target=".update-modal" 
            title="Update"><span>&#xf044</span></a>
            <a data-delete_id="$row->id" data-delete_url="$row->url" 
            class="btn btn-dark m-1 fa-input open-modal-delete" 
            data-toggle="modal" 
            data-target=".delete-modal" 
            title="Delete"><span>&#xf2ed</span></a>
            </td>
            </tr>
            EOD;
                $i++;
        }
        $output .= '
        <tr><td colspan="12">
        <div class="row col-md-8">' . $pag_output . '</div>
        </td></tr>
        </tbody>
    </table>
    ';
}

ob_start();
$image->content($output);
