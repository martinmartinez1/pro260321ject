<!-- update modal -->
<div class="modal update-modal" role="dialog" aria-labelledby="UpdateModal" aria-hidden="true" id="update-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p>Edit img data</p>
                <button class="close" data-dismiss="modal">×</button>
            </div>
            <table class="table table-responsive">
                <form class="p-3" action="
                <?php htmlspecialchars($_SERVER['PHP_SELF']) ?>
                " enctype="multipart/form-data" method="POST">
                    <tr>
                        <td colspan="2">
                            <img src="" id="old_url_img" class="img-fluid">
                            <p>to change this image just choose another one below</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="alt">Alt:</label>
                                <textarea class="form-control" type="text" name="alt" id="alt" value="" rows="3" cols="45" placeholder="Alt" maxlength="125">
                                </textarea>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input class="form-control" type="text" name="title" id="title" value="" placeholder="Title" maxlength="60">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="position">position on listing</label>
                                <input type="number" name="position" id="position" value="" min="1" max="9999">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="is_active">is active</label><br>
                                <input type="checkbox" name="is_active" id="is_active" value="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="file" accept="image/*" name="image" id="image">
                                <input type="hidden" 
                                name="old_url" id="old_url">
                                <input type="hidden" 
                                name="old_img_width_hidden" id="old_img_width_hidden">
                                <input type="hidden" 
                                name="old_img_height_hidden" id="old_img_height_hidden">
                                <input type="hidden" 
                                name="old_img_size_hidden" id="old_img_size_hidden">
                                <input type="hidden" 
                                name="old_img_mime_hidden" id="old_img_mime_hidden">
                                <input type="hidden" name="id" id="id">
                            </div>
                        </td>
                        <td>
                            <input type="submit" class="btn btn-dark m-1 fa-input" value="&#xf00c Submit" name="update">
                            <button class="btn btn-light" data-dismiss="modal">Cancel</button>
                        </td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>
<!-- update modal -->