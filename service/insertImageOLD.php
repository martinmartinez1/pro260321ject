<?php

namespace service;

use model\Image;

require_once 'model/Image.php';

$image = new Image();

$imgArr = getimagesize($_FILES['image']['tmp_name']);
$width = $imgArr[0];
$height = $imgArr[1];
$size = $_FILES['image']['size'];

if ($size < 10485760) {

    $url = Image::saveFile();

    if ($_POST) {
        $data = array(
            'url'       =>  $image->setUrl($url),
            'alt'       =>  $image->setAlt($_POST['alt']),
            'title'     =>  $image->setTitle($_POST['title']),
            'onlisting' =>  $image->setOnListing($_POST['onlisting']),
            'isactive'  =>  $image->setIsActive(isset($_POST['isactive']) ? true : false),
            'width'     =>  $image->setWidth($width),
            'height'    =>  $image->setHeight($height),
            'size'      =>  $image->setSize($_FILES['image']['size']),
            'mime'      =>  $image->setMime($_FILES['image']['type'])
        );
    } else {
        echo '
        <div class="alert alert-danger">
        unable to catch data from uploaded file
        </div>
        ';
    }
    $image->insert();
    echo '
    <div class="alert alert-success">
    uploaded
    </div>
    ';
} else {
    echo '
    <div class="alert alert-danger">
    only files up to 10 MB
    </div>
    ';
}

echo "<a href=\"javascript:history.go(-1)\">GO BACK</a>";
