<?php

namespace service;

require_once 'model/Image.php';

if (isset($_POST['update'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $process = $image->saveFile();
    if (null == $process) {
        // if no img:
        $url = $_POST['old_url'];
        $image->setUrl($url);
        $width  = $_POST['old_img_width_hidden'];
        $height = $_POST['old_img_height_hidden'];
        $size   = $_POST['old_img_size_hidden'];
        $mime   = $_POST['old_img_mime_hidden'];
    } else {
        // if img
        $size = $process[0];
        $image->setUrl($process[1]);
        $url_old = $_POST['old_url'];
        $imgArr = getimagesize('upload/standard/' . $_FILES['image']['name']);
        $width = $imgArr[0];
        $height = $imgArr[1];
        $mime = $_FILES['image']['type'];
    }

    $image->insertUpdateStuff($size, $width, $height, $mime);
    $image->setId(intval($_POST['id']));
    $image->update($image->getId());
    $image->removeFile($url_old);
    header("Refresh:1");
    if ((null != $process) || ($_FILES['image']['size'] < $image->getAllowedSize())) {
        ob_clean();
        echo '<div class="alert alert-success">
            Updated. Wait...
            </div>';
    } else {
        ob_clean();
        echo '<div class="alert alert-danger">
            Only files up to 10 MB.  Wait...
            </div>';
    }
}
