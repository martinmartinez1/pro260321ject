<?php

namespace service;

use Error;

require_once 'model/Image.php';

if (isset($_POST['delete'])) {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $id = $_POST['delete_id'];
    $url = $_POST['delete_url'];

    try {
        $image->delete($id);
        $image->removeFile($url);
        ob_clean();
        header("Refresh:3");
        echo '
            <div class="alert alert-success">
            Img succesfully deleted.  Wait...
            </div>
            ';
    } catch (Error $e) {
        ob_clean();
        header("Refresh:3");
        echo '
        <div class="alert alert-danger">
        It was some issue, not deleted.  Wait...
        </div>
        ';
    }
}
